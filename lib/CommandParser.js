function CommandParser() {

    // make sure a new instance is returned
    if (this.__proto__ != CommandParser.prototype) {
        var cur = this.__proto__.super;
        while (cur) {
            if (cur != CommandParser) {
                cur = cur.prototype.super;
            }
        }
        return new CommandParser();
    }
}

CommandParser.prototype.call = function (command) {
    console.log("Command: " + command);
    return "Fabulus: " + command;
};

module.exports = CommandParser;