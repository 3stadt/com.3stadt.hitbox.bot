require('dotenv').load();
HitboxChatClient = require("hitbox-chat");
CommandParser = require("./lib/CommandParser");

// Convenience function
Array.prototype.foreach = function (callback) {
    for (var k = 0; k < this.length; k++) {
        callback(k, this[k]);
    }
};

var channels = JSON.parse(process.env.CHANNELS);
var username = process.env.HITBOX_USERNAME;
var pass = process.env.HITBOX_PASSWORD;

var client = HitboxChatClient({name: username, pass: pass});
var parser = CommandParser();
var connectedChannels = [];

var joinTime = null;

client.on("connect", function () {
    console.log("connected!");
    channels.foreach(function (k, channelName) {
        var channel = client.joinChannel(channelName);

        console.log(channel);

        channel.on("login", function (name, role) {
            /*
             * successfully joined channel
             * role is one of {
             *   guest: read-only (bad or no credentials)
             *   anon: normal read/write
             *   user: mod
             *   admin: owner/staff
             * }
             */
            if (name == username) {
                if (role == "guest") {
                    console.log("Something went wrong: Was logged in as guest.");
                    process.exit();
                }
                joinTime = Date.now();
            }
            console.log("joined as " + name);
        }).on("chat", function (name, text, role) {
            if (name != username) {
                if (text.charAt(0) == "!") {
                    var dif = Date.now() - joinTime;
                    if (dif / 1000 > 10) { // do not take commands within 10 seconds after join
                        if (text == "!quit") {
                            process.exit();
                        }
                        var response = parser.call(text);
                        console.log("Response: " + response);
                        channel.sendMessage(response, null);
                    }
                } else {
                    channel.sendMessage("--> " + text, null);
                }
            }
        });
        connectedChannels.push(channel);
    });
});